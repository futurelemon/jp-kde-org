# jp.kde.org

Japan KDE Users Group Website
日本 KDE ユーザ会 Web サイト

## ローカル開発環境のセットアップ ― Ruby 環境で開発する場合

### 環境要件

- Ruby 2.5.0 以上 ([最新の Jekyll がサポートする Ruby](https://jekyllrb.com/docs/installation/#requirements))
- [docker-compose](https://docs.docker.com/compose/install/) (リダイレクトに関してテストをする場合のみ)

### `jekyll serve` コマンドによるテスト

このリポジトリーのルートディレクトリに移動し、`bundle install` で依存パッケージをインストールした後、`jekyll serve` コマンドで開発用サーバーを起動して下さい。通常は http://127.0.0.1:4000/ でアクセスできるようになるはずです。

```shell
$ cd /path/to/jp.kde.org/
$ bundle config set --local path "vendor/bundle"
$ bundle install
$ bundle exec jekyll serve
```

### `docker-compose` によるテスト

.htaccess のテストを行うためには、docker-compose で Apache 環境を立ち上げる必要があります。

このリポジトリーのルートディレクトリーに移動し、`bundle install` で依存パッケージをインストールした後、`jekyll build` コマンドでビルドを行って下さい。その後、`docker-compose up` を実行することで、Apache の環境が起動します。Docker コンテナの外側から、http://localhost:4000 にアクセスすることで、ページを表示することができます。

```shell
$ cd /path/to/jp.kde.org/
$ bundle config set --local path "vendor/bundle"
$ bundle install
$ bundle exec jekyll build
$ docker-compose up
```

## デプロイログの確認

デプロイは、master ブランチに変更がマージされたタイミングで、Jenkins によって自動的に行われます。  
以下のページからログの確認が可能です。

https://binary-factory.kde.org/view/Websites/job/Website_jp-kde-org/
